require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const mysql = require("mysql");

app.use(bodyParser.json());

const pool = mysql.createPool({
  connectionLimit: 10,
  host: process.env.host,
  user: process.env.user,
  password: process.env.password,
  database: process.env.database
});

//Select all data
app.get("/mahasiswa", (req, res) => {
  pool.getConnection((err, connection) => {
    if (err) throw err;

    connection.query("SELECT * from mahasiswa", (err, rows) => {
      connection.release();
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    });
  });
});

// Select data
app.get("/mahasiswa/:id", (req, res) => {
  pool.getConnection((err, connection) => {
    if (err) throw err;
    connection.query(
      "SELECT * FROM mahasiswa WHERE id_mahasiswa = ?",
      [req.params.id],
      (err, rows) => {
        connection.release();
        if (!err) {
          res.send(rows);
        } else {
          console.log(err);
        }
      }
    );
  });
});

// Post data
app.post("/mahasiswa", (req, res) => {
  pool.getConnection((err, connection) => {
    if (err) throw err;

    const params = req.body;
    connection.query("INSERT INTO mahasiswa SET ?", params, (err, rows) => {
      connection.release();
      if (!err) {
        res.send(`Berhasil menambahakan data mahasiswa.`);
      } else {
        console.log(err);
      }
    });
  });
});

//Update data
app.put("/mahasiswa/:id", (req, res) => {
  pool.getConnection((err, connection) => {
    if (err) throw err;

    const { nim, nama } = req.body;
    let id_mahasiswa = req.params.id;

    connection.query(
      "UPDATE mahasiswa SET nim = ?, nama = ? WHERE id_mahasiswa = ?",
      [nim, nama, id_mahasiswa],
      (err, rows) => {
        connection.release();

        if (!err) {
          res.send(`Data berhasil diupdate.`);
        } else {
          console.log(err);
        }
      }
    );
  });
});

// Delete data
app.delete("/mahasiswa/:id", (req, res) => {
  pool.getConnection((err, connection) => {
    if (err) throw err;
    connection.query(
      "DELETE FROM mahasiswa WHERE id_mahasiswa = ?",
      [req.params.id],
      (err, rows) => {
        connection.release();
        if (!err) {
          res.send(`Data berhasil di hapus.`);
        } else {
          console.log(err);
        }
      }
    );
  });
});

//port
app.listen(3000, () => {
  console.log("Server started on port 3000...");
});
